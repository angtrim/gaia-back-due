var express = require('express');
var router = express.Router();
var mongoose = require('mongoose');
var flaredb = require('../flaredb.js');


router.post('/', function(req, res, next) {
    var body = req.body;
    var beaconid2 = body.id;
    console.log(beaconid2);
    var userId = 1;
    flaredb.Thing.find({
            id1: beaconid2
        },
        function(err, thingFound) {
            if (err) return next(err);
            // Update user status
            console.log(thingFound);
            var newTrees = thingFound[0].data.trees;
            var newCo2 = thingFound[0].data.trees + 5;
            updateUser(userId, newTrees, newCo2, function(update) {
                var response = {
                    success: true,
                    update: update
                }
                res.json(response);
            });


        });
});


function updateUser(userId, newTrees, newCo2, callback) {

    flaredb.User.find({
            id: userId
        },
        function(err, usersFound) {
            if (err) return next(err);
            var userFound = usersFound[0];
            var oldTrees = userFound.total_trees;
            var oldCo2 = userFound.total_co2;
            console.log(userFound);
            var query = {
                'id': userId
            };
            var updatedSet = {
                total_trees: oldTrees + newTrees,
                total_co2: oldCo2 + newCo2
            }
            console.log(updatedSet);
            flaredb.User.update(query, {
                $set: updatedSet
            }, function(error) {
                var update = {
                    "activity_name": "Bike Sharing",
                    "total_trees": updatedSet.total_trees,
                    "total_co2": updatedSet.total_co2,
                    "new_trees": newTrees,
                    "new_co2": newCo2
                }
                callback(update);
            });
        });


}


module.exports = router;